# coding: utf-8
# Configuration
import logging

LOG_LEVEL = logging.INFO
#LOG_LEVEL = logging.DEBUG

# Redis connection
REDIS_HOST = "localhost"
REDIS_PORT = 6379

# Redis channels to monitor and kafka topics where to produce
REDIS_CHANNELS = {
    "__keyevent@0__:expired": "redis-keys-expired"
}

# Slack
SLACK_TOKEN = "xoxp-18390195648-18396570839-82306411701-9e181dd089d2a0ba9fac7c1ad5265515"
SLACK_USER = 'konocloud-redis-listener'

# Kafka
KAFKA_URI = 'worker1.konodrac.local:6667'
